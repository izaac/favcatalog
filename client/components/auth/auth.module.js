'use strict';

angular.module('favcatalogApp.auth', [
  'favcatalogApp.constants',
  'favcatalogApp.util',
  'ngCookies',
  'ui.router'
])
  .config(function($httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');
  });
