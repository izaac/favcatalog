'use strict';

angular.module('favcatalogApp.admin', [
  'favcatalogApp.auth',
  'ui.router'
]);
