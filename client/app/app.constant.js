(function(angular, undefined) {
'use strict';

angular.module('favcatalogApp.constants', [])

.constant('appConfig', {userRoles:['guest','user','admin']})

;
})(angular);